
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:03PM

See merge request itentialopensource/adapters/adapter-imperva!11

---

## 0.3.3 [08-27-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-imperva!9

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:11PM

See merge request itentialopensource/adapters/adapter-imperva!8

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:24PM

See merge request itentialopensource/adapters/adapter-imperva!7

---

## 0.3.0 [05-15-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/security/adapter-imperva!6

---

## 0.2.4 [03-27-2024]

* Changes made at 2024.03.27_13:40PM

See merge request itentialopensource/adapters/security/adapter-imperva!5

---

## 0.2.3 [03-11-2024]

* Changes made at 2024.03.11_16:13PM

See merge request itentialopensource/adapters/security/adapter-imperva!4

---

## 0.2.2 [02-27-2024]

* Changes made at 2024.02.27_11:46AM

See merge request itentialopensource/adapters/security/adapter-imperva!3

---

## 0.2.1 [01-25-2024]

* Patch/adapt 3022

See merge request itentialopensource/adapters/security/adapter-imperva!2

---

## 0.2.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-imperva!1

---

## 0.1.2 [06-08-2022]

* Bug fixes and performance improvements

See commit af4106d

---

## 0.1.1 [06-07-2022]

* Bug fixes and performance improvements

See commit 3c90bfc

---

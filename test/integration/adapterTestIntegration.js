/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-imperva',
      type: 'Imperva',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Imperva = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Imperva Adapter Test', () => {
  describe('Imperva Class Tests', () => {
    const a = new Imperva(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const rulesSiteId = 555;
    const rulesPostSitesSiteIdRulesBodyParam = {
      rule_id: 2,
      name: 'string',
      action: 'RULE_ACTION_BLOCK_USER',
      filter: 'string',
      response_code: 3,
      add_missing: false,
      from: 'string',
      to: 'string',
      rewrite_name: 'string',
      dc_id: 2,
      port_forwarding_context: 'string',
      port_forwarding_value: 'string',
      rate_context: 'IP',
      rate_interval: 9,
      error_type: 'error.type.ssl_failed',
      error_response_format: 'json',
      error_response_data: {
        incidentId: '$INCIDENT_ID$',
        hostName: '$HOST_NAME$',
        errorCode: '$RR_CODE$',
        description: '$RR_DESCRIPTION$',
        timeUtc: '$TIME_UTC$',
        clientIp: '$CLIENT_IP$',
        proxyId: '$PROXY_ID$',
        proxyIp: '$PROXY_IP$'
      },
      multiple_deletions: true,
      overrideWafRule: 'string',
      overrideWafAction: 'string'
    };
    describe('#postSitesSiteIdRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSitesSiteIdRules(rulesSiteId, rulesPostSitesSiteIdRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'postSitesSiteIdRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rulesRuleId = 555;
    const rulesPostSitesSiteIdRulesRuleIdBodyParam = {
      rule_id: 9,
      name: 'string',
      action: 'RULE_ACTION_RESPONSE_REWRITE_RESPONSE_CODE',
      filter: 'string',
      response_code: 3,
      add_missing: true,
      from: 'string',
      to: 'string',
      rewrite_name: 'string',
      dc_id: 7,
      port_forwarding_context: 'string',
      port_forwarding_value: 'string',
      rate_context: 'IP',
      rate_interval: 6,
      error_type: 'error.type.all',
      error_response_format: 'xml',
      error_response_data: {
        incidentId: '$INCIDENT_ID$',
        hostName: '$HOST_NAME$',
        errorCode: '$RR_CODE$',
        description: '$RR_DESCRIPTION$',
        timeUtc: '$TIME_UTC$',
        clientIp: '$CLIENT_IP$',
        proxyId: '$PROXY_ID$',
        proxyIp: '$PROXY_IP$'
      },
      multiple_deletions: true,
      overrideWafRule: 'string',
      overrideWafAction: 'string'
    };
    describe('#postSitesSiteIdRulesRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSitesSiteIdRulesRuleId(rulesSiteId, rulesRuleId, rulesPostSitesSiteIdRulesRuleIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'postSitesSiteIdRulesRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rulesPutSitesSiteIdRulesRuleIdBodyParam = {
      rule_id: 6,
      name: 'string',
      action: 'RULE_ACTION_REWRITE_HEADER',
      filter: 'string',
      response_code: 6,
      add_missing: true,
      from: 'string',
      to: 'string',
      rewrite_name: 'string',
      dc_id: 5,
      port_forwarding_context: 'string',
      port_forwarding_value: 'string',
      rate_context: 'IP',
      rate_interval: 6,
      error_type: 'error.type.no_ssl_config',
      error_response_format: 'xml',
      error_response_data: {
        incidentId: '$INCIDENT_ID$',
        hostName: '$HOST_NAME$',
        errorCode: '$RR_CODE$',
        description: '$RR_DESCRIPTION$',
        timeUtc: '$TIME_UTC$',
        clientIp: '$CLIENT_IP$',
        proxyId: '$PROXY_ID$',
        proxyIp: '$PROXY_IP$'
      },
      multiple_deletions: false,
      overrideWafRule: 'string',
      overrideWafAction: 'string'
    };
    describe('#putSitesSiteIdRulesRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSitesSiteIdRulesRuleId(rulesSiteId, rulesRuleId, rulesPutSitesSiteIdRulesRuleIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'putSitesSiteIdRulesRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesSiteIdRulesRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesSiteIdRulesRuleId(rulesSiteId, rulesRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'getSitesSiteIdRulesRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsExtSiteId = 555;
    describe('#postSitesExtSiteIdSettingsGeneralAdditionalTxtRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(settingsExtSiteId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'postSitesExtSiteIdSettingsGeneralAdditionalTxtRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsSiteId = 555;
    const settingsPostSitesSiteIdSettingsMaskingBodyParam = {
      hashing_enabled: true,
      hash_salt: 'string'
    };
    describe('#postSitesSiteIdSettingsMasking - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSitesSiteIdSettingsMasking(settingsSiteId, settingsPostSitesSiteIdSettingsMaskingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'postSitesSiteIdSettingsMasking', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSitesExtSiteIdSettingsGeneralAdditionalTxtRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(settingsExtSiteId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'putSitesExtSiteIdSettingsGeneralAdditionalTxtRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesExtSiteIdSettingsGeneralAdditionalTxtRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(settingsExtSiteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'getSitesExtSiteIdSettingsGeneralAdditionalTxtRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesSiteIdSettingsMasking - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesSiteIdSettingsMasking(settingsSiteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'getSitesSiteIdSettingsMasking', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customCertificateWithHSMSupportExtSiteId = 555;
    const customCertificateWithHSMSupportPutSitesExtSiteIdHsmCertificateBodyParam = {
      data: {
        certificate: 'LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSLOfd4d6gjFsRENDQW55Z0F3SUJBZ0lKQU1sSDZPNzB3b2FvTUEwR0NTcUdTSWIzRFFFQkN3VUFNR2t4Q3pBSkJnTlYKQkFZVEFrbE1NUTh3RFFZRFZRUUlEQVpKYzNKaFpXd3hFREFPQmdOVkJBY01CM0psYUc5MmIzUXhFakFRQmdOVgpCQW9NQ1UxNVEyOXRjR0Z1ZVRFTU1Bb0dBMVVFQ3d3RFpHVjJNUlV3RXdZRFZRUUREQXhwYm1OaGNIUmxjM1F1ClkyOHdIaGNOTWpFd09EQXlNRGt6TURJNVdoY05Nak13T0RBeU1Ea3pNREk1V2pCcE1Rc3dDUVlEVlFRR0V3SkoKVERFUE1BMEdBMVVFQ0F3R1NYTnlZV1ZzTVJBd0RnWURWUVFIREFkeVpXaHZkbTkwTVJJd0VBWURWUVFLREFsTgplVU52YlhCaGJua3hEREFLQmdOVkJBc01BMlJsZGpFVk1CTUdBMVVFQXd3TWFXNWpZWEIwWlhOMExtTnZNSUlCCklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUE3M2JGcXI3bTRtNmMvenc0dDREK2lINkMKYmdzenExTnZqcFArOG01NDk2U01RNlh3dCsyN21SWlM4TGRqbDBCV2VFWCtHTUlTUkc0aElPa0hYMkpTUlBScAozdUUrOXFOaVhOQ3lwZkxiZENSZHVDRnl0MER6WmtIUVRPZXZpL3JsN0p6ZXREZGZDd2R4dFlHNG40ZzM0VnFDClY2NkZHWXkvRFU0WERkeW5neFhscVRRZ0VRMGd5eThnUm9VR2VIb2N2Mi8wdFgxOUdWOFI5Q0FlaVlWOURGMHQKZ1VqY0VwcEJ3NDIwTG4wOW9JN2JEbjNYdFJMYWVZbk4vRytnbW5TVWp6NjByMTg2ZVVCV2MrK3N2dXc3Tmt3MwpsUTgwdGw2UTZOVU4vbjhqY0ZPczgvOGNpVDVxM0VxcXlLdlRhSXdJYVJZUEpEUjRZS1NYb3UvZ0lBZ1RYUUlECkFRQUJvejh3UFRBTEJnTlZIUThFQkFNQ0JEQXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0V3R1FZRFZSMFIKQkJJd0VJSU9LaTVwYm1OaGNIUmxjM1F1WTI4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFLT0xVUkZodldzZwpYeis1aUkwWTZaSnk2c0FOaGZIWTM5TTFrcytwMXBkZ1pkOHhKMFplenZNT000Yi80QjVmTWR5eXp2QUdMemgvCmtIZFFEU3NQUlRkcDVJT1RQd1VSNW9SN3owYjZpV2JWTTVBTW5SbHhkUHFydWhMc0JuZncxVG5MalVrYWtRSUgKS3hPenczdzdSZ24xaEsxZHVrUVc3WTdnem0vWnJuTXUwYkkvUnhrc1JLblhleGpnZXNybWZXWGtRSEowQXAxNQpEcjF3OTVrL1ZPbVBjcjViYTRIK1dkTURnTDhRYUpndGprZjdyd01MdkJDU1piR1FIQVArU2QyOEoyNVIwSXVrCjN2Y0dLMHM4UFoxeGFtU1BWU2VLaVZHNVU4OXdjb0FXTEEwS24zSVJDczRvNnptODE2QUNXb3pQM2g3bGlVSGUKaG9WS1BFZU1LbUk9Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0=',
        hsmDetails: [
          {
            keyId: '257r65d8-9d62-8l16-91g2-7g64345278n2',
            apiKey: samProps.authentication.password,
            hostName: 'api.amer.smartkey.io'
          }
        ]
      }
    };
    describe('#putSitesExtSiteIdHsmCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSitesExtSiteIdHsmCertificate(customCertificateWithHSMSupportExtSiteId, customCertificateWithHSMSupportPutSitesExtSiteIdHsmCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomCertificateWithHSMSupport', 'putSitesExtSiteIdHsmCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesExtSiteIdHsmCertificateConnectivityTest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesExtSiteIdHsmCertificateConnectivityTest(customCertificateWithHSMSupportExtSiteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomCertificateWithHSMSupport', 'getSitesExtSiteIdHsmCertificateConnectivityTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customCertificateExtSiteId = 555;
    const customCertificatePutSitesExtSiteIdCustomCertificateBodyParam = {
      certificate: 'LS0tLS1CRUdJTiBDRVJU...WS1BFZU1LbUk9Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0=',
      private_key: samProps.authentication.password,
      passphrase: '1234',
      auth_type: 'RSA'
    };
    describe('#putSitesExtSiteIdCustomCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSitesExtSiteIdCustomCertificate(customCertificateExtSiteId, customCertificatePutSitesExtSiteIdCustomCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomCertificate', 'putSitesExtSiteIdCustomCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesSiteIdRulesRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesSiteIdRulesRuleId(rulesSiteId, rulesRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'deleteSitesSiteIdRulesRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(settingsExtSiteId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecordsDeleteAll - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecordsDeleteAll(settingsExtSiteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecordsDeleteAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesExtSiteIdHsmCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesExtSiteIdHsmCertificate(customCertificateWithHSMSupportExtSiteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomCertificateWithHSMSupport', 'deleteSitesExtSiteIdHsmCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customCertificateAuthType = 'fakedata';
    describe('#deleteSitesExtSiteIdCustomCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesExtSiteIdCustomCertificate(customCertificateExtSiteId, customCertificateAuthType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomCertificate', 'deleteSitesExtSiteIdCustomCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesSiteIdSettingsCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesSiteIdSettingsCache(555, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralCacheSettings', 'getSitesSiteIdSettingsCache', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalCacheSettingsPutSitesSiteIdSettingsCacheBodyParam = {
      mode: {
        level: 'standard',
        https: 'include_html',
        time: 5
      },
      key: {
        unite_naked_full_cache: true,
        comply_vary: true
      },
      response: {
        stale_content: {
          mode: 'custom',
          time: 5
        },
        cache_shield: false,
        cache_response_header: {
          mode: 'disabled',
          headers: [
            'string'
          ]
        },
        tag_response_header: 'string',
        cache_empty_responses: false,
        cache_300x: true,
        cache_http_10_responses: false,
        cache_404: {
          enabled: false,
          time: 9
        }
      },
      ttl: {
        use_shortest_caching: false,
        prefer_last_modified: true
      },
      client_side: {
        enable_client_side_caching: true,
        comply_no_cache: true,
        send_age_header: false
      }
    };
    describe('#putSitesSiteIdSettingsCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSitesSiteIdSettingsCache(555, generalCacheSettingsPutSitesSiteIdSettingsCacheBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralCacheSettings', 'putSitesSiteIdSettingsCache', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesSiteIdSettingsCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesSiteIdSettingsCache(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralCacheSettings', 'deleteSitesSiteIdSettingsCache', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesSiteIdCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesSiteIdCache(555, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralCacheSettings', 'deleteSitesSiteIdCache', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesSiteIdCacheXray - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesSiteIdCacheXray(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralCacheSettings', 'getSitesSiteIdCacheXray', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cacheRulesPostSitesSiteIdSettingsCacheRulesBodyParam = {
      name: 'string',
      action: 'HTTP_CACHE_ADD_TAG',
      enabled: false,
      filter: 'string'
    };
    describe('#postSitesSiteIdSettingsCacheRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSitesSiteIdSettingsCacheRules(555, cacheRulesPostSitesSiteIdSettingsCacheRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CacheRules', 'postSitesSiteIdSettingsCacheRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesSiteIdSettingsCacheRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesSiteIdSettingsCacheRules(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CacheRules', 'getSitesSiteIdSettingsCacheRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesSiteIdSettingsCacheRulesRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesSiteIdSettingsCacheRulesRuleId(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CacheRules', 'getSitesSiteIdSettingsCacheRulesRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cacheRulesPutSitesSiteIdSettingsCacheRulesRuleIdBodyParam = {
      name: 'string',
      action: 'HTTP_CACHE_IGNORE_AUTH_HEADER',
      enabled: false,
      filter: 'string'
    };
    describe('#putSitesSiteIdSettingsCacheRulesRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSitesSiteIdSettingsCacheRulesRuleId(555, 555, cacheRulesPutSitesSiteIdSettingsCacheRulesRuleIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CacheRules', 'putSitesSiteIdSettingsCacheRulesRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesSiteIdSettingsCacheRulesRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesSiteIdSettingsCacheRulesRuleId(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CacheRules', 'deleteSitesSiteIdSettingsCacheRulesRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesSiteIdSettingsDelivery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesSiteIdSettingsDelivery(555, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Delivery', 'getSitesSiteIdSettingsDelivery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deliveryPutSitesSiteIdSettingsDeliveryBodyParam = {
      compression: {
        file_compression: true,
        minify_js: false,
        minify_css: false,
        minify_static_html: false
      },
      image_compression: {
        compress_jpeg: false,
        progressive_image_rendering: true,
        aggressive_compression: true,
        compress_png: false
      },
      network: {
        tcp_pre_pooling: false,
        origin_connection_reuse: true,
        support_non_sni_clients: false,
        enable_http2: false,
        http2_to_origin: false,
        port: {
          to: 'string',
          from: 'string'
        },
        ssl_port: {
          to: 'string',
          from: 'string'
        }
      },
      redirection: {
        redirect_naked_to_full: true,
        redirect_http_to_https: false
      },
      custom_error_page: {
        error_page_template: 'string',
        custom_error_page_templates: {
          'error.type.connection_timeout': 'string',
          'error.type.access_denied': 'string',
          'error.type.parse_req_error': 'string',
          'error.type.parse_resp_error': 'string',
          'error.type.connection_failed': 'string',
          'error.type.ssl_failed': 'string',
          'error.type.deny_and_captcha': 'string',
          'error.type.no_ssl_config': 'string'
        }
      }
    };
    describe('#putSitesSiteIdSettingsDelivery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSitesSiteIdSettingsDelivery(555, deliveryPutSitesSiteIdSettingsDeliveryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Delivery', 'putSitesSiteIdSettingsDelivery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSitesSiteIdSettingsDelivery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSitesSiteIdSettingsDelivery(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Delivery', 'deleteSitesSiteIdSettingsDelivery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSites - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSites('accountId', '10', '3', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteManagement', 'listSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSitesDataCentersConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSitesDataCentersConfiguration(123, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteManagement', 'getSitesDataCentersConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setSitesDataCentersConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSitesDataCentersConfiguration(123, {}, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-imperva-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteManagement', 'setSitesDataCentersConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});

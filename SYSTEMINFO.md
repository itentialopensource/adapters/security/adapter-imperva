# Imperva

Vendor: Imperva
Homepage: https://www.imperva.com

Product: Imperva
Product Page: https://www.imperva.com

## Introduction
We classify Imperva into the Security domain as Imperva provides access to comprehensive security solutions and functionalities contributing to the protection, monitoring and management of assets and data. 

"Imperva provides centralized data security across legacy and modern cloud environments by automating detection, protection, and risk response for compliance and security operations" 

## Why Integrate
The Imperva adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Imperva to deliver security-related functionalities to address diverse security challenges.

With this adapter you have the ability to perform operations with Imperva such as:

- Site Management
- Rules
- Configure, manage, and monitor WAF policies and security settings

## Additional Product Documentation
The [API documents for Imperva](https://docs.imperva.com/bundle/api-docs/page/api/api-overview.htm)

## 0.1.2 [06-08-2022]

* Bug fixes and performance improvements

See commit af4106d

---

## 0.1.1 [06-07-2022]

* Bug fixes and performance improvements

See commit 3c90bfc

---

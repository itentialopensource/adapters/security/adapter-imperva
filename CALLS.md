## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Imperva. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Imperva.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Imperva. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">postSitesSiteIdRules(siteId, rule, callback)</td>
    <td style="padding:15px">Create rule</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitesSiteIdRulesRuleId(siteId, ruleId, rule, callback)</td>
    <td style="padding:15px">Overwrite rule - must contain valid rule id</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesSiteIdRulesRuleId(siteId, ruleId, callback)</td>
    <td style="padding:15px">Read rule - must contain valid rule id</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSitesSiteIdRulesRuleId(siteId, ruleId, rule, callback)</td>
    <td style="padding:15px">Update rule - must contain valid rule id</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesSiteIdRulesRuleId(siteId, ruleId, callback)</td>
    <td style="padding:15px">Delete rule - must contain valid rule id</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesSiteIdSettingsMasking(siteId, callback)</td>
    <td style="padding:15px">Returns a masking setting for the given site.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/masking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSitesSiteIdSettingsMasking(siteId, maskingSettings, callback)</td>
    <td style="padding:15px">Update masking settings for site</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/masking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(extSiteId, callback)</td>
    <td style="padding:15px">Return all TXT records defined for the site in Cloud WAF</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/general/additionalTxtRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(extSiteId, txtRecordValueOne, txtRecordValueTwo, txtRecordValueThree, txtRecordValueFour, txtRecordValueFive, callback)</td>
    <td style="padding:15px">Create or modify one or more of the TXT records defined for the site in Cloud WAF  (partial update)</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/general/additionalTxtRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(extSiteId, recordNumber, txtRecordValue, callback)</td>
    <td style="padding:15px">Overwrite a specific TXT record that is defined for the site in Cloud WAF  (full update)</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/general/additionalTxtRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecords(extSiteId, recordNumber, callback)</td>
    <td style="padding:15px">Delete a specific TXT record that is defined for the site in Cloud WAF</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/general/additionalTxtRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesExtSiteIdSettingsGeneralAdditionalTxtRecordsDeleteAll(extSiteId, callback)</td>
    <td style="padding:15px">Delete all TXT records that are defined for the site in Cloud WAF</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/general/additionalTxtRecords/delete-all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitesExtSiteIdHsmCertificate(extSiteId, data, callback)</td>
    <td style="padding:15px">Upload custom certificate and HSM credentials</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/hsmCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesExtSiteIdHsmCertificate(extSiteId, callback)</td>
    <td style="padding:15px">Remove custom certificate and HSM credentials</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/hsmCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesExtSiteIdHsmCertificateConnectivityTest(extSiteId, callback)</td>
    <td style="padding:15px">Test connectivity between Imperva and HSM provider</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/hsmCertificate/connectivityTest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitesExtSiteIdCustomCertificate(extSiteId, body, callback)</td>
    <td style="padding:15px">Upload custom certificate</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/customCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesExtSiteIdCustomCertificate(extSiteId, authType, callback)</td>
    <td style="padding:15px">Remove custom certificate</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/customCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesSiteIdSettingsCache(siteId, sections = 'mode', callback)</td>
    <td style="padding:15px">Get cache settings</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitesSiteIdSettingsCache(siteId, body, callback)</td>
    <td style="padding:15px">Change cache settings</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesSiteIdSettingsCache(siteId, callback)</td>
    <td style="padding:15px">Restore default cache settings</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesSiteIdCache(siteId, urlPattern, tags, callback)</td>
    <td style="padding:15px">Purge a site's cache</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/cache?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesSiteIdCacheXray(siteId, callback)</td>
    <td style="padding:15px">Refresh and get a site's XRAY access URL.</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/cache/xray?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSitesSiteIdSettingsCacheRules(siteId, rules, callback)</td>
    <td style="padding:15px">Create cache rules</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesSiteIdSettingsCacheRules(siteId, callback)</td>
    <td style="padding:15px">List all cache rules for specific site</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesSiteIdSettingsCacheRulesRuleId(siteId, ruleId, callback)</td>
    <td style="padding:15px">List cache rule - must contain valid rule id</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitesSiteIdSettingsCacheRulesRuleId(siteId, ruleId, rule, callback)</td>
    <td style="padding:15px">Update cache rule - partial update - must contain valid rule id</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesSiteIdSettingsCacheRulesRuleId(siteId, ruleId, callback)</td>
    <td style="padding:15px">Delete cache rule - must contain valid rule id</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/cache/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesSiteIdSettingsDelivery(siteId, sections = 'compression', callback)</td>
    <td style="padding:15px">Get delivery settings</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/delivery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSitesSiteIdSettingsDelivery(siteId, body, callback)</td>
    <td style="padding:15px">Change delivery settings</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/delivery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSitesSiteIdSettingsDelivery(siteId, callback)</td>
    <td style="padding:15px">Restore default delivery settings</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}/settings/delivery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSites(accountId, pageSize, pageNum, callback)</td>
    <td style="padding:15px">List sites for an account</td>
    <td style="padding:15px">{base_path}/v1/sites/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSitesDataCentersConfiguration(extSiteId, callback)</td>
    <td style="padding:15px">Use this operation to get configured data centers and all their servers</td>
    <td style="padding:15px">{base_path}/v3/sites/{pathv1}/data-centers-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSitesDataCentersConfiguration(extSiteId, body, callback)</td>
    <td style="padding:15px">Use this operation to configure site's data centers and all their servers</td>
    <td style="padding:15px">{base_path}/v3/sites/{pathv1}/data-centers-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Imperva System. The API that was used to build the adapter for Imperva is usually available in the report directory of this adapter. The adapter utilizes the Imperva API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Imperva adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Imperva to deliver security-related functionalities to address diverse security challenges.

With this adapter you have the ability to perform operations with Imperva such as:

- Site Management
- Rules
- Configure, manage, and monitor WAF policies and security settings

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 

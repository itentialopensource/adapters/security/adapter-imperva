## Authenticating Imperva Adapter 

This document will go through the steps for authenticating the Imperva adapter with Static Token Authentication. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

Companies periodically change authentication methods to provide better security. As this happens this section should be updated and contributed/merge back into the adapter repository.

### Static Token Authentication
The Imperva adapter requires Static Token Authentication. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Imperva server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. To authenticate with Imperva service API ID and API key have to be sent in request headers: https://docs.imperva.com/bundle/api-docs/page/api/authentication.htm
4. The keys can be obtained via imperva portal: https://docs.imperva.com/bundle/cloud-application-security/page/settings/api-keys.htm
5. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "static_token",
  "auth_field": [
    "header.headers.x-API-Id",
    "header.headers.x-API-Key"
  ],
  "auth_field_format": [
    "myAPiId",
    "myAPIKey"
  ],
  "auth_logging": false
}
```
you can leave all of the other properties in the authentication section, they will not be used for static token authentication.

4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct x-API-Id and x-API-Key.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
